# Bio Kekule

Aquest projecte pertany a l'activitat [Bioinformática - Molécules](https://xtec.dev/bio/molecule/)


```sh
docker run --rm -d --name kekule -p 80:80 registry.gitlab.com/xtec/bio-kekule
```

## Develop

Install dependencies and close terminal :

```sh
./init.sh
source ~/.bashrc
```


Run:

```sh
npm run dev
```

or:

```sh
npm run next-dev
npm run flask-dev
```
