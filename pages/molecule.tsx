import { Kekule } from 'kekule';
import { useEffect } from 'react';

export default function Molecule() {

  useEffect(() => {
    const mol = createMolecule()
    drawMolecule(mol)
  });

  return <div id="canvas"></div>

}

function createMolecule() {
  // create molecule first
  var mol = new Kekule.Molecule();
  // add three atoms to molecule, property setter can be called cascadely
  var a1 = (new Kekule.Atom()).setSymbol('C').setCoord2D({ 'x': -0.4, 'y': 0.23 });
  var a2 = (new Kekule.Atom()).setSymbol('C').setCoord2D({ 'x': 0.4, 'y': 0.23 });
  var a3 = (new Kekule.Atom()).setSymbol('O').setCoord2D({ 'x': 0, 'y': -0.46 });
  mol.appendNode(a1);
  mol.appendNode(a2);
  mol.appendNode(a3);
  // add three bonds to molecule
  var b1 = (new Kekule.Bond()).setBondOrder(1).setConnectedObjs([a1, a2]);
  var b2 = (new Kekule.Bond()).setBondOrder(1).setConnectedObjs([a2, a3]);
  var b3 = (new Kekule.Bond()).setBondOrder(1).setConnectedObjs([a3, a1]);
  mol.appendConnector(b1);
  mol.appendConnector(b2);
  mol.appendConnector(b3);

  return mol

}

var drawBoxWidth = 500;
var drawBoxHeight = 500;


function drawMolecule(mol:any) {

  const condensed = true;
  const canvas = document.getElementById('canvas');;
  var bridge = new Kekule.Render.CanvasRendererBridge();

  var context = bridge.createContext(canvas, drawBoxWidth, drawBoxHeight);

  var baseCoord = { 'x': drawBoxWidth / 2, 'y': drawBoxHeight / 2 };
  var options = {
    moleculeDisplayType: condensed ? Kekule.Render.MoleculeDisplayType.CONDENSED : Kekule.Render.MoleculeDisplayType.SKELETAL,
    retainAspect: true,
    autoScale: true,
    zoom: 1
  };

  bridge.clearContext(context);

  var painter = new Kekule.Render.ChemObjPainter(Kekule.CoordMode.COORD2D, mol, bridge);
  painter.draw(context, baseCoord, options);

}